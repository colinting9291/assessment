package com.example.person.dao;

public interface BaseDAO<T> {
	/**
	 * 
	 * @param id
	 * @return
	 */
	public T getById(Object id);

	/**
	 * 
	 * @param entity
	 */
	public void insert(T entity);

	/**
	 * 
	 * @param entity
	 * @return
	 */
	public T update(T entity);

	/**
	 * 
	 * @param entity
	 */
	public void delete(T entity);
}
