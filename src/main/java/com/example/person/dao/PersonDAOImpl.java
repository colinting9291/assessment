/**
 * 
 */
package com.example.person.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.person.entity.Person;

/**
 * @author colin.ting
 *
 */
@Repository
@Transactional
public class PersonDAOImpl implements PersonDAO {

	@PersistenceContext
	EntityManager em;

	private static Logger logger = LogManager.getLogger(PersonDAOImpl.class);

	@Override
	public Person retrieveByFirstName(String firstName) {
		logger.debug("retrieveByFirstName");
		Person bean = null;
		String query = "SELECT user FROM Person user WHERE user.firstName = :firstName";
		try {
			bean = em.createQuery(query, Person.class)
					.setParameter("firstName", firstName).getSingleResult();

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return bean;
	}

	@Override
	public List<Person> retrieveAllPersons() {
		logger.debug("retrieveAllPersons");
		List<Person> list = new ArrayList<>();
		String query = "SELECT a FROM Person a";
		try {
			list = em.createQuery(query, Person.class).getResultList();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return list;
	}

	@Override
	public Person getById(Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(Person entity) {
		em.persist(entity);

	}

	@Override
	public Person update(Person entity) {
		Person person = em.merge(entity);
		return person;
	}

	@Override
	public void delete(Person entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public int updatePerson(String firstName, String lastName, String email) {
		int update = 0;
		String query = "UPDATE Person SET lastName = :lastName, email = :email WHERE firstName = :firstName";
		try {
			Query q = em
					.createQuery(query);
			update = q.setParameter("lastName", lastName).setParameter("email", email)
					.setParameter("firstName", firstName).executeUpdate();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return update;
	}

	@Override
	public int deletePerson(String firstName) {
		int update = 0;
		String query = "DELETE FROM Person WHERE firstName = :firstName";
		try {
			Query q = em
					.createQuery(query);
			update = q.setParameter("firstName", firstName).executeUpdate();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return update;
	}
}
