package com.example.person.dao;

import java.util.List;

import com.example.person.entity.Person;
/**
 * 
 * @author colin.ting
 *
 */
public interface PersonDAO extends BaseDAO<Person>{
	/**
	 * 
	 * @param userName
	 * @return
	 */
	public Person retrieveByFirstName(String firstName);

	/**
	 * 
	 * @return
	 */
	public List<Person> retrieveAllPersons();
	
	/**
	 * 
	 * @param firstName
	 * @return
	 */
	public int updatePerson(String firstName, String lastName, String email);
	
	/**
	 * 
	 * @param firstName
	 * @return
	 */
	public int deletePerson(String firstName);
}
