/**
 * 
 */
package com.example.person.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.person.entity.Person;
import com.example.person.service.PersonService;


/**
 * @author colin.ting
 *
 */
@RestController
public class PersonController {

	private static final Logger logger = LogManager.getLogger(PersonController.class);
	
	@Autowired
	PersonService personService;
	
	@RequestMapping(value = "/getPersonByFirstName/{firstName}", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getPersonByFirstName(@PathVariable("firstName") String firstName, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		HttpStatus httpStatus = HttpStatus.OK;
		Person person = null;
		try {
			person = personService.getPersonByUsername(firstName);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			map.put("result", person);
		}

		return new ResponseEntity<Map<String, Object>>(map, httpStatus);
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getAllPersons(HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		HttpStatus httpStatus = HttpStatus.OK;
		List<Person> list = new ArrayList<>();
		
		try {
			list = personService.getAllPersons();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			map.put("result", list.size());
		}
		
		return new ResponseEntity<Map<String, Object>>(map, httpStatus);
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> insertPerson(@RequestBody Person person, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		HttpStatus httpStatus = HttpStatus.OK;
		
		try {
			personService.createPerson(person);;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			map.put("result", "ok");
		}
		
		return new ResponseEntity<Map<String, Object>>(map, httpStatus);
	}
	
	@RequestMapping(value = "/test", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> updatePerson(@RequestBody Person person, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		HttpStatus httpStatus = HttpStatus.OK;
		String firstName = "";
		String lastName = "";
		String email = "";
		int update = 0;
		try {
			firstName = person.getFirstName();
			lastName = person.getLastName();
			email = person.getEmail();
			update = personService.updatePerson(firstName, lastName, email);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			map.put("result", update);
		}
		
		return new ResponseEntity<Map<String, Object>>(map, httpStatus);
	}
	
	@RequestMapping(value = "/{firstName}", method = RequestMethod.DELETE)
	public ResponseEntity<Map<String, Object>> deletePerson(@PathVariable("firstName") String firstName, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		HttpStatus httpStatus = HttpStatus.OK;
		int update = 0;
		try {
			update = personService.deletePersonByUsername(firstName);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
			map.put("result", update);
		}
		
		return new ResponseEntity<Map<String, Object>>(map, httpStatus);
	}
}
