package com.example.person.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.context.request.RequestContextListener;



/**
 * 
 * @author colin.ting
 *
 */
@Configuration
@ComponentScan
@EnableScheduling
public class ApplicationConfiguration {

	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}

}
