package com.example.person.exception;

import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

/**
 * 
 * @author colin.ting
 *
 */
public class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

	private static final Logger logger = LogManager.getLogger(CustomAsyncExceptionHandler.class);

	@Override
	public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {

		logger.error("Exception message - {}", throwable.getMessage());
		logger.error("Method name - {}", method.getName());
		for (Object param : obj) {
			logger.error("Parameter value - {}", param);
		}
	}

}
