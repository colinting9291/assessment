/**
 * 
 */
package com.example.person.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.person.entity.Person;

/**
 * @author colin.ting
 *
 */
@Repository
public interface PersonService {

	public Person getPersonByUsername(String firstName);
	
	public int deletePersonByUsername(String firstName);
	
	public void createPerson(Person person);
	
	public List<Person> getAllPersons();
	
	public int updatePerson(String firstName, String lastName, String email);
}
