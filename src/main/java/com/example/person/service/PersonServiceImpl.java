/**
 * 
 */
package com.example.person.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.person.dao.PersonDAO;
import com.example.person.entity.Person;

/**
 * @author colin.ting
 *
 */
@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	PersonDAO personDAO;

	@Override
	public Person getPersonByUsername(String firstName) {
		Person person = personDAO.retrieveByFirstName(firstName);
		return person;
	}

	@Override
	public int deletePersonByUsername(String firstName) {
		return personDAO.deletePerson(firstName);
	}

	@Override
	public void createPerson(Person person) {
		personDAO.insert(person);
		
	}

	@Override
	public List<Person> getAllPersons() {
		List<Person> list = personDAO.retrieveAllPersons();
		return list;
	}

	@Override
	public int updatePerson(String firstName, String lastName, String email) {
		return personDAO.updatePerson(firstName, lastName, email);
	}


}
